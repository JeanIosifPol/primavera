***PRODUCT***

GET http://localhost:8084/api/product?index=1
GET http://localhost:8084/api/product/all
POST http://localhost:8084/api/admin/product
{
    "name" : "Krokodyla skóra",
    "price" : "234",
    "available" : true

}
PUT http://localhost:8084/api/admin/product
{
    "id" : 3,
    "name" : "Lwia skóra",
    "price" : "2340",
    "available" : false

}
PATCH http://localhost:8084/api/admin/product
{
    "id" :3,
    "available" : true
}


****CUSTOMER****

GET http://localhost:8084/api/customer?index=1
GET http://localhost:8084/api/customer/all
POST http://localhost:8084/api/admin/customer
{
    "name" : "Agamemnon",
    "addres" : "Mykeny"
}
PUT http://localhost:8084/api/admin/customer
{
    "id" : "3",
    "name" : "Ajgistos",
    "addres" : "Mykeny"

}
PATCH http://localhost:8084/api/admin/customer
{
    "id" :3,
    "name":"Orestes"

}

*****ORDER*****

GET http://localhost:8084/api/order?index=1
GET http://localhost:8084/api/order/all
POST http://localhost:8084/api/admin/order 
{
    
    "status" : "in progress"

}
PUT http://localhost:8084/api/admin/order
{
    "id":5,
    "status" : "in progress"
    
}
PATCH http://localhost:8084/api/admin/order
{
    "id" :3,
    "status": "done"
   

}
