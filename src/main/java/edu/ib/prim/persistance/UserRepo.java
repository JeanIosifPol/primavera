package edu.ib.prim.persistance;


import edu.ib.prim.persistance.entity.UserDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends CrudRepository<UserDto, Long> {
}
