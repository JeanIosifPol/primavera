package edu.ib.prim.persistance.entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String addres;

//    @OneToMany(fetch = FetchType.LAZY)
//    @JoinColumn(name = "customer_id")
//    private List<Order> orders;

    public Customer() {
    }

    public Customer(String name, String addres) {
        this.name = name;
        this.addres = addres;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddres() {
        return addres;
    }

    public void setAddres(String addres) {
        this.addres = addres;
    }
}
