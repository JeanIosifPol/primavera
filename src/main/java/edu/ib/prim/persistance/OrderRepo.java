package edu.ib.prim.persistance;

import edu.ib.prim.persistance.entity.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepo extends CrudRepository<Order, Long> {
}
