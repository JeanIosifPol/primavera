package edu.ib.prim;

import edu.ib.prim.PasswordEncoderConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import javax.sql.DataSource;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    private PasswordEncoderConfig passwordEncoderConfig;

    @Autowired
    private DataSource dataSource;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .usersByUsernameQuery("select u.name, u.password_hash, 1 from users u where u.name=?")
                .authoritiesByUsernameQuery("select u.name, u.role, 1 from users u where u.name=?")
                .dataSource(dataSource)
                .passwordEncoder(passwordEncoderConfig.passwordEncoder());
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic().and().authorizeRequests()
                .antMatchers(HttpMethod.GET, "/api/product").hasAnyRole("CUSTOMER", "ADMIN")
                .antMatchers(HttpMethod.GET, "/api/product/all").hasAnyRole("CUSTOMER", "ADMIN")
                .antMatchers(HttpMethod.GET, "/api/orders").hasAnyRole("CUSTOMER", "ADMIN")
                .antMatchers(HttpMethod.GET, "/api/orders/all").hasAnyRole("CUSTOMER", "ADMIN")
                .antMatchers(HttpMethod.POST, "/api/orders").hasAnyRole("CUSTOMER", "ADMIN")
                .antMatchers(HttpMethod.GET, "/api/customer").hasRole("CUSTOMER")
                .antMatchers(HttpMethod.GET, "/api/customer/all").hasRole("CUSTOMER")
                .antMatchers(HttpMethod.POST, "/api/admin/product").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/api/admin/product").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH, "/api/admin/product").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/api/admin/customer").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/api/admin/customer").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH, "/api/admin/customer").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/api/admin/orders").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH, "/api/admin/orders").hasRole("ADMIN")
                .and()
                .formLogin().permitAll()
                .and()
                .logout().permitAll()
                .and()
                .csrf().disable();
    }
}
