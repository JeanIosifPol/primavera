package edu.ib.prim.api;

import edu.ib.prim.manager.CustomerManager;
import edu.ib.prim.persistance.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class CustomerAPI {

    private CustomerManager customers;
    @Autowired
    public CustomerAPI(CustomerManager customers) {
        this.customers = customers;
    }

    @GetMapping("/customer/all")
    public Iterable<Customer> getAll(){
        return customers.findAll();
    }

    @GetMapping("/customer")
    public Optional<Customer> getById(@RequestParam Long index){
       return customers.findById(index);
    }
    @PostMapping("admin/customer")
    public Customer addCust(@RequestBody Customer customer){
        return customers.save(customer);
    }

    @PutMapping("admin/customer")
    public Customer updateCustomer(@RequestBody Customer customer){ //faktycznie musi byc tutaj caly Customer (a nie samo id), by modyfikacja miala racje bytu
        return customers.save(customer);
    }
    @PatchMapping("admin/customer")
    public void deleteCustomer(@RequestBody Customer customer){
        customers.save(customer);
    }


}





