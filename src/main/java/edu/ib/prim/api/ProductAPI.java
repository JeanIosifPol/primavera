package edu.ib.prim.api;


import edu.ib.prim.manager.ProductManager;

import edu.ib.prim.persistance.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


@RestController
@RequestMapping("/api")
public class ProductAPI {

    private ProductManager products;
    @Autowired
    public ProductAPI(ProductManager customers) {
        this.products = customers;
    }

    @GetMapping("/product/all")
    public Iterable<Product> getAll(){
        return products.findAll();
    }

    @GetMapping("/product")
    public Optional<Product> getById(@RequestParam Long index){
        return products.findById(index);
    }
    @PostMapping("admin/product")
    public Product addProd(@RequestBody Product product){
        return products.save(product);
    }

    @PutMapping("admin/product")
    public Product updateProduct(@RequestBody Product product){
        return products.save(product);
    }
   


}
