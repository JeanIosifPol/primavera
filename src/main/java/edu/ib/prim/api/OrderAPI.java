package edu.ib.prim.api;

import edu.ib.prim.manager.OrderManager;
import edu.ib.prim.persistance.entity.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class OrderAPI {

    private OrderManager orders;

    @Autowired
    public OrderAPI(OrderManager orders) {
        this.orders = orders;
    }

    @GetMapping("/order/all")
    public Iterable<Order> getAll() {
        return orders.findAll();
    }

    @GetMapping("/order")
    public Optional<Order> getById(@RequestParam Long index) {
        return orders.findById(index);
    }

    @PostMapping("admin/order")
    public Order addOrd(@RequestBody Order order) {
        return orders.save(order);
    }

    @PutMapping("admin/order")
    public Order updateOrder(@RequestBody Order order) {
        return orders.save(order);
    }

    @PatchMapping("admin/order")
    public void patchOder(@RequestBody Order order) {
        orders.save(order);
    }
}