package edu.ib.prim.manager;


import edu.ib.prim.persistance.OrderRepo;

import edu.ib.prim.persistance.entity.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class OrderManager {
    private OrderRepo orderRepo;
    @Autowired
    public OrderManager(OrderRepo customerRepo){
        this.orderRepo = customerRepo;
    }
    public Optional<Order> findById(Long id){
        return orderRepo.findById(id);
    }
    public Iterable<Order> findAll(){
        return orderRepo.findAll();
    }

    public Order save(Order customer){
        return orderRepo.save(customer);
    }


}
