package edu.ib.prim.manager;

import edu.ib.prim.persistance.CustomerRepo;
import edu.ib.prim.persistance.ProductRepo;
import edu.ib.prim.persistance.entity.Customer;
import edu.ib.prim.persistance.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProductManager {
    private ProductRepo productRepo;
    @Autowired
    public ProductManager(ProductRepo productRepo){
        this.productRepo = productRepo;
    }
    public Optional<Product> findById(Long id){
        return productRepo.findById(id);
    }
    public Iterable<Product> findAll(){
        return productRepo.findAll();
    }

    public Product save(Product customer){
        return productRepo.save(customer);
    }


}
