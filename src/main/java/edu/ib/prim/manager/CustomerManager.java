package edu.ib.prim.manager;

import edu.ib.prim.persistance.CustomerRepo;
import edu.ib.prim.persistance.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerManager {
    private CustomerRepo customerRepo;
    @Autowired
    public CustomerManager(CustomerRepo customerRepo){
        this.customerRepo = customerRepo;
    }
    public Optional<Customer> findById(Long id){
        return customerRepo.findById(id);
    }
    public Iterable<Customer> findAll(){
        return customerRepo.findAll();
    }

    public Customer save(Customer customer){
        return customerRepo.save(customer);
    }

}



