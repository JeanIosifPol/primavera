package edu.ib.prim.manager;

import edu.ib.prim.persistance.UserRepo;

import edu.ib.prim.persistance.entity.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;



@Service
public class UserManager {
    private UserRepo productRepo;
    @Autowired
    public UserManager(UserRepo productRepo){
        this.productRepo = productRepo;
    }
    public Optional<UserDto> findByName(Long id){
        return productRepo.findById(id);
    }

    public Iterable<UserDto> findAll(){
        return productRepo.findAll();
    }

    public UserDto save(UserDto customer){
        return productRepo.save(customer);
    }


}
