package edu.ib.prim;

import edu.ib.prim.persistance.entity.UserDto;


public class UserDtoBuilder {

        public UserDto addUser(User user){
            PasswordEncoderConfig passwordEncoderConfig = new PasswordEncoderConfig();
            String password = (passwordEncoderConfig.passwordEncoder().encode(user.getPassword()));
            UserDto userDto = new UserDto(user.getName(),password,user.getRole());
            return userDto;
        }
}
