package edu.ib.prim;

import edu.ib.prim.persistance.OrderRepo;
import edu.ib.prim.persistance.ProductRepo;
import edu.ib.prim.persistance.UserRepo;
import edu.ib.prim.persistance.entity.*;
import edu.ib.prim.persistance.CustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Component
public class DbMockData {
    private ProductRepo productRepository;
    private OrderRepo orderRepository;
    private CustomerRepo customerRepository;
    private UserRepo userRepository;

    @Autowired
    public DbMockData(ProductRepo productRepository, OrderRepo orderRepository, CustomerRepo customerRepository, UserRepo userRepository) {
        this.productRepository = productRepository;
        this.orderRepository = orderRepository;
        this.customerRepository = customerRepository;
        this.userRepository = userRepository;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fill() {
        Product product = new Product("Zlote runo", 255f, true);
        Product product1 = new Product("Kaczy puch", 500f, true);
        Customer customer = new Customer("Eurysteusz", "Mykeny");
        Customer customer1 = new Customer("Atreus", "Mykeny");
        User user = new User("user", "user1", "ROLE_CUSTOMER");
        User user1 = new User("admin", "admin1", "ROLE_ADMIN");
        UserDtoBuilder udb = new UserDtoBuilder();

        UserDto us = udb.addUser(user);
        UserDto us1 = udb.addUser(user1);
        Set<Product> products = new HashSet<>() {
            {
                add(product);
                add(product1);
            }};
        Order order = new Order(customer, products, LocalDateTime.now(), "in progress");
        Order order1 = new Order(customer1, products, LocalDateTime.now(), "in progress");

        productRepository.save(product);
        productRepository.save(product1);
        customerRepository.save(customer);
        customerRepository.save(customer1);
        orderRepository.save(order);
        orderRepository.save(order1);
        userRepository.save(us);
        userRepository.save(us1);

    }
}
